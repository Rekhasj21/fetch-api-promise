import fetch from "node-fetch";

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(todos => {
        console.log(todos[0]);
        const userId = todos[0].userId;
        return Promise.all([todos[0], fetch(`https://jsonplaceholder.typicode.com/users?id=${userId}`)]);
    })
    .then(([firstTodo, usersResponse]) => Promise.all([firstTodo, usersResponse.json()]))
    .then(([firstTodo, users]) => console.log([firstTodo, users[0]]))
    .catch(error => console.error(error));
