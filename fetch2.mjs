import fetch from "node-fetch"

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(todosResponse => todosResponse.json())
    .then(todosDetails => console.log(todosDetails))
    .catch(err => console.error(err.message))
