import fetch from "node-fetch"

fetch('https://jsonplaceholder.typicode.com/users')
    .then(usersResponse => usersResponse.json())
    .then(usersDetails => console.log(usersDetails))
    .catch(err => console.error(err.message))
