import fetch from "node-fetch"

function getUserTodoList([usersDetails, todoList]) {
    const eachUserTodoDetails = []
    usersDetails.forEach(user => {
        const userDetails = todoList.filter(todo => todo.userId === user.id)
        eachUserTodoDetails.push({ user: user.name, todos: userDetails })
    })
    console.log(JSON.stringify(eachUserTodoDetails))
}

fetch('https://jsonplaceholder.typicode.com/users')
    .then(usersResponse => usersResponse.json())
    .then((usersDetails) => {
        return Promise.all([usersDetails, fetch("https://jsonplaceholder.typicode.com/todos")])
    })
    .then(([usersDetails, todoList]) => Promise.all([usersDetails, todoList.json()]))
    .then(([usersDetails, todoList]) => getUserTodoList([usersDetails, todoList])
    ).catch(err => console.error(err.message))
