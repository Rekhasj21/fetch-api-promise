import fetch from "node-fetch";

fetch('https://jsonplaceholder.typicode.com/users')
    .then(usersResponse => usersResponse.json())
    .then(usersDetails => {
        return Promise.all([usersDetails, fetch('https://jsonplaceholder.typicode.com/todos')])
    })
    .then(([usersDetails, todosResponse]) => {
        return Promise.all([usersDetails, todosResponse.json()])
    })
    .then(([usersDetails, todosDetails]) => console.log(usersDetails,todosDetails))
    .catch(err => console.error(err.message))
